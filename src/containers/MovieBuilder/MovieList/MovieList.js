import React from 'react';
import MovieItem from "../../../components/MovieItem/MovieItem";
import './MovieList.css';

const MovieList = props => {
  const movieItem = props.movies.map(movie => (
      <MovieItem
          key={movie.id}
          title={movie.title}
          remove={() => props.removeItem(movie.id)}
          change={(event) => props.changeItem(event, movie.id)}
          edit={() => props.editItem()}
      />
  ));
  return (
      <div className="movie__list">
        <h3 className="list__heading">To watch list:</h3>
        {movieItem}
      </div>
  );
};

export default MovieList;