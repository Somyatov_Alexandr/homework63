import React, { Component } from 'react';
import MovieAdd from "./MovieAdd/MovieAdd";
import MovieList from "./MovieList/MovieList";
import './MovieBuilder.css';
import axios from 'axios';

class MovieBuilder extends Component {
  state = {
    movies: [],
    loading: false
  };

  componentDidMount() {
    axios.get('/movies.json').then((response) => {
      const movies = [];

      for(let key in response.data) {
        movies.push({...response.data[key], id: key});
      }
      this.setState({movies})
    }).catch(error => {
      console.log(error);
    })
  }

  removeMovieItem = id => {
    axios.delete(`/movies/${id}.json`).then(() => {
      this.setState(prevState => {
        const movies = [...prevState.movies];
        const index = movies.findIndex(movie => movie.id === id);
        movies.splice(index, 1);

        return {movies}
      })
    });
  };

  handleChangeMovieItem = (event, id) => {
    let moviesCopy = [...this.state.movies];
    const index = moviesCopy.findIndex(item => item.id === id);

    let movieCopy = moviesCopy[index];
    movieCopy.title = event.target.value;
    moviesCopy[index] = movieCopy;

    this.setState({movies: moviesCopy});
  };

  updateMoviesHandler = () => {
    this.setState({loading: true});
    axios.post('/movies.json', this.state.movies).then(() => {
      this.setState({loading: false});
    });
  };

  render() {
    return (
        <div className="movie">
          <MovieAdd/>
          <MovieList
              movies={this.state.movies}
              removeItem={this.removeMovieItem}
              changeItem={this.handleChangeMovieItem}
              editItem={this.updateMoviesHandler}
          />
        </div>
    );
  }
}

export default MovieBuilder;