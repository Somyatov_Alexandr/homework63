import React, {Component} from 'react';
import './MovieAdd.css';
import axios from "axios/index";

class MovieAdd extends Component {
  state = {
    movie: {
      title: ''
    },
    loading: false
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState({loading: true});

    axios.post('/movies.json', this.state.movie).then(() => {
      this.setState({loading: false});
    });
    // this.props.history.replace('/');
    event.target.reset();
  };

  handleMovieTitle = event => {
    event.persist();
    this.setState(prevState => {
      return {
        movie: {...prevState.movie, [event.target.name]: event.target.value}
      }
    });
  };

  render () {
    return (
        <div className="movie__add">
          <form className="movie__form" onSubmit={this.handleSubmit}>
            <input
                type="text"
                className="movie__input"
                placeholder="Add movie, which you want to watch"
                name="title"
                onChange={this.handleMovieTitle}
                required
            />
            <button className="movie__btn">Add</button>
          </form>
        </div>
    );
  }
}

export default MovieAdd;