import React, {Component, Fragment} from 'react';
import {NavLink, Route, Switch} from "react-router-dom";
import './App.css';

import MovieBuilder from "./containers/MovieBuilder/MovieBuilder";

class App extends Component {
  render() {
    return (
      <Fragment>
        <menu className="app-nav">
          <li className="app-nav__item"><NavLink to="/" exact>Movies</NavLink></li>
          <li className="app-nav__item"><NavLink to="/todo-list" exact>TodoList</NavLink></li>
        </menu>
        <Switch>
          <Route to="/" exact component={MovieBuilder}/>
          {/*<Route to="/todo-list" exact component={TodoList}/>*/}
          <Route render={() => <h1>Page Not found</h1>}/>
        </Switch>
      </Fragment>
    );
  }
}

export default App;
